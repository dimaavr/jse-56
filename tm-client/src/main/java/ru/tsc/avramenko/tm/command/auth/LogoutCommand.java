package ru.tsc.avramenko.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.SessionEndpoint;

@Component
public class LogoutCommand extends AbstractCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "User logout from system.";
    }

    @Override
    public void execute() {
        @Nullable final boolean logout = sessionEndpoint.closeSession(sessionService.getSession());
        if (logout) sessionService.setSession(null);
    }

}