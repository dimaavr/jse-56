package ru.tsc.avramenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.endpoint.AdminDataEndpoint;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.Session;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import java.util.Optional;

@Component
public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "data-xml-jaxb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load xml data from file (JaxB).";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        adminDataEndpoint.loadDataXmlJaxB(session);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}