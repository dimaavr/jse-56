package ru.tsc.avramenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DisplayCommand extends AbstractCommand {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @Nullable
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list commands.";
    }

    @Override
    public void execute() {
        int index = 1;
        final HashMap<String, String> cmd = new HashMap<>();
        for (final AbstractCommand command : commandService.getCommands()) {
            cmd.put(command.name(), command
                    .getClass()
                    .getPackage()
                    .getName()
                    .substring(command.getClass().getPackage().getName().lastIndexOf(".")+1).toUpperCase());
        }
        final Map<String, List<String>> valueMap = cmd.keySet().stream().collect(Collectors.groupingBy(k -> cmd.get(k)));
        for(String s : valueMap.keySet()) {
            System.out.println("\n" + s + " COMMAND:");
            for (String str : valueMap.get(s)) {
                System.out.println(index + ". " + str);
                index++;
            }
        }
    }

}