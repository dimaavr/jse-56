package ru.tsc.avramenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.endpoint.AdminDataEndpoint;
import ru.tsc.avramenko.tm.endpoint.Role;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import java.util.Optional;

@Component
public class BackupSaveCommand extends AbstractDataCommand {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    public final static String BACKUP_SAVE = "backup-save";

    @NotNull
    @Override
    public String name() {
        return BACKUP_SAVE;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Backup data.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        adminDataEndpoint.saveBackup(session);
    }

    @Nullable
    public Role[] roles() {
        if (sessionService.getSession() == null || Thread.currentThread().getName().equals("DataThread")) {
            return null;
        }
        else {
            return new Role[]{Role.ADMIN};
        }
    }

}