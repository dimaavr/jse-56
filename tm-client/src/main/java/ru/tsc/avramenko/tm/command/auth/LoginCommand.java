package ru.tsc.avramenko.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.endpoint.SessionDTO;
import ru.tsc.avramenko.tm.endpoint.SessionEndpoint;
import ru.tsc.avramenko.tm.util.TerminalUtil;

@Component
public class LoginCommand extends AbstractCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login user to system.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionEndpoint.openSession(login, password);
        sessionService.setSession(session);
        System.out.println("Welcome back, " + login + "! \n" +
                "Use '" + commandService.getCommandByName("help").name() + "' to see more commands!");
    }

}