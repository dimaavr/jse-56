package ru.tsc.avramenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import java.util.Collection;

@Component
public class ArgumentsDisplayCommand extends AbstractCommand {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @Nullable
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> arguments = commandService.getArguments();
        for (final AbstractCommand argument : arguments)
            System.out.println(argument.arg());
    }

}