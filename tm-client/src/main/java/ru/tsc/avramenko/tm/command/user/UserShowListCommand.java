package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.endpoint.*;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;

import java.util.List;
import java.util.Optional;

@Component
public class UserShowListCommand extends AbstractUserCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("List of users.");
        @Nullable final List<UserDTO> users = adminUserEndpoint.findAllUser(session);
        if (users == null) throw new ProcessException();
        int index = 1;
        for (final UserDTO user : users) {
            System.out.println("\n" + "|--- User [" + index + "]---|");
            showUser(user);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}