package ru.tsc.avramenko.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;
import java.util.jar.Manifest;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String FILE_SCANNER_INTERVAL_DEFAULT = "10";

    @NotNull
    private static final String FILE_SCANNER_INTERVAL_KEY = "scanner.interval";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    private String getValue(final String name, final String defaultValue) {
        @Nullable
        final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable
        final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultValue);
    }

    private int getValueInt(final String name, final String defaultValue) {
        @Nullable
        final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable
        final String environmentProperty = System.getenv(name);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(name, defaultValue));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getFileScannerInterval() {
        return getValueInt(FILE_SCANNER_INTERVAL_KEY, FILE_SCANNER_INTERVAL_DEFAULT);
    }

}