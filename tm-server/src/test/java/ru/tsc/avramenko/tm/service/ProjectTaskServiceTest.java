package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.configuration.ServerConfiguration;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.service.model.*;

import java.util.List;

public class ProjectTaskServiceTest {

    @Nullable
    private static ProjectTaskService projectTaskService;

    @Nullable
    private static ProjectService projectService;

    @Nullable
    private static TaskService taskService;

    @Nullable
    private static SessionService sessionService;

    @Nullable
    private static UserService userService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @Nullable
    private Project project;

    @Nullable
    private Task task;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    private static AnnotationConfigApplicationContext context;

    @BeforeClass
    public static void beforeClass() {
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        sessionService = context.getBean(SessionService.class);
        taskService = context.getBean(TaskService.class);
        projectService = context.getBean(ProjectService.class);
        projectTaskService = context.getBean(ProjectTaskService.class);
        userService = context.getBean(UserService.class);
        userService.create("Test1", "Test1");
    }

    @Before
    public void before() {
        this.session = sessionService.open("Test1", "Test1");
        projectService.create(session.getUser().getId(), TEST_PROJECT_NAME, TEST_DESCRIPTION_NAME);
        this.project = projectService.findByName(session.getUser().getId(), TEST_PROJECT_NAME);
        taskService.create(session.getUser().getId(), TEST_TASK_NAME, TEST_DESCRIPTION_NAME);
        this.task = taskService.findByName(session.getUser().getId(), TEST_TASK_NAME);
    }

    @After
    public void after() {
        taskService.removeByName(session.getUser().getId(), task.getName());
        projectTaskService.removeProjectByName(session.getUser().getId(), project.getName());
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertNull(task.getProject());
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, task.getDescription());

        @Nullable final Task taskById = taskService.findById(session.getUser().getId(), task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());

        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @Nullable final Project projectById = projectService.findById(session.getUser().getId(), project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    public void findTaskByProjectId() {
        @Nullable final List<Task> tasks = projectTaskService.findTaskByProjectId(session.getUser().getId(), project.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void bindTaskById() {
        @NotNull final Task bindTask = projectTaskService.bindTaskById(session.getUser().getId(), project.getId(), task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNotNull(bindTask.getProject().getId());
        Assert.assertEquals(bindTask.getProject().getId(), project.getId());
    }

    @Test
    public void unbindTaskById() {
        @NotNull final Task bindTask = projectTaskService.unbindTaskById(session.getUser().getId(), project.getId(), task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNull(bindTask.getProject());
    }

}