package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.configuration.ServerConfiguration;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.service.model.SessionService;
import ru.tsc.avramenko.tm.service.model.UserService;

import java.util.List;

public class SessionServiceTest {

    @Nullable
    private static SessionService sessionService;

    @Nullable
    private static UserService userService;

    @NotNull
    private static Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_USER_LOGIN = "TestLogin";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "TestUserPassword";

    @NotNull
    protected static final String TEST_USER_PASSWORD_INCORRECT = "TestUserPasswordIncorrect";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "647";

    @NotNull
    private static AnnotationConfigApplicationContext context;

    @BeforeClass
    public static void beforeClass() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        sessionService = context.getBean(SessionService.class);
        userService = context.getBean(UserService.class);
        userService.create("Test2", "Test2");
    }

    @Before
    public void before() {
        this.session = sessionService.open("Test2", "Test2");
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUser());

        @Nullable final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    public void findById() {
        @Nullable final Session session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final Session session = sessionService.findById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test
    public void findAllByUserId() {
        @Nullable final List<Session> session = sessionService.findAll();
        Assert.assertNotNull(session);
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @Nullable final List<Session> session = sessionService.findAll();
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD_INCORRECT);
        Assert.assertNotNull(session);
    }

    @Test
    public void validate() {
        sessionService.validate(session);
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        session.setSignature("647");
        sessionService.validate(session);
    }

}