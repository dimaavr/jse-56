package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.avramenko.tm.api.service.dto.ITaskDtoService;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.repository.dto.TaskDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TaskDtoService extends AbstractService implements ITaskDtoService {

    private EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.add(task);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.add(task);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            return taskDtoRepository.findByName(userId, name);
        } finally {
            taskDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            return taskDtoRepository.findByIndex(userId, index);
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.removeByName(userId, name);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.removeByIndex(userId, index);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @NotNull final TaskDTO task = Optional.ofNullable(taskDtoRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    @Nullable
    public TaskDTO findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            return taskDtoRepository.findById(userId, id);
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    public @Nullable void removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.removeById(userId, id);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable String userId) {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            return taskDtoRepository.findAllById(userId);
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    public @Nullable List<TaskDTO> findAll() {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            return taskDtoRepository.findAll();
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.clear(userId);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    public void clear() {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.clear();
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Override
    public void addAll(@Nullable List<TaskDTO> tasks) {
        if (tasks == null) return;
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            for (@NotNull final TaskDTO task : tasks) taskDtoRepository.add(task);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

}