package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.avramenko.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.avramenko.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyIndexException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.repository.dto.ProjectDtoRepository;
import ru.tsc.avramenko.tm.repository.dto.TaskDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskDtoService extends AbstractService implements IProjectTaskDtoService {

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            return taskDtoRepository.findAllTaskByProjectId(userId, projectId);
        } finally {
            taskDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @Nullable final TaskDTO task = taskDtoRepository.findById(userId, taskId);
            task.setProjectId(projectId);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            @Nullable final TaskDTO task = taskDtoRepository.findById(userId, taskId);
            task.setProjectId(null);
            taskDtoRepository.update(task);
            taskDtoRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        @NotNull IProjectDtoRepository projectDtoRepository = context.getBean(ProjectDtoRepository.class);
        try {
            taskDtoRepository.getTransaction().begin();
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeById(userId, projectId);
            taskDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        @NotNull IProjectDtoRepository projectDtoRepository = context.getBean(ProjectDtoRepository.class);
        try {
            projectDtoRepository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectDtoRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeByIndex(userId, index);
            projectDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            projectDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            projectDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull ITaskDtoRepository taskDtoRepository = context.getBean(TaskDtoRepository.class);
        @NotNull IProjectDtoRepository projectDtoRepository = context.getBean(ProjectDtoRepository.class);
        try {
            projectDtoRepository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectDtoRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            taskDtoRepository.unbindAllTaskByProjectId(userId, projectId);
            projectDtoRepository.removeByName(userId, name);
            projectDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            projectDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            projectDtoRepository.close();
        }
    }

}