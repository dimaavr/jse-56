package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.model.ITaskRepository;
import ru.tsc.avramenko.tm.api.repository.model.IUserRepository;
import ru.tsc.avramenko.tm.api.service.model.ITaskService;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.model.TaskRepository;
import ru.tsc.avramenko.tm.repository.model.UserRepository;
import ru.tsc.avramenko.tm.service.AbstractService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService extends AbstractService implements ITaskService {

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        @NotNull final IUserRepository userRepository = context.getBean(UserRepository.class);
        @Nullable final User user = userRepository.findById(userId);
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.add(task);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        @NotNull final IUserRepository userRepository = context.getBean(UserRepository.class);
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final User user = userRepository.findById(userId);
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.add(task);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            return taskRepository.findByName(userId, name);
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            return taskRepository.findByIndex(userId, index);
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.removeByName(userId, name);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.removeByIndex(userId, index);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status);
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            @NotNull final Task task = Optional.ofNullable(taskRepository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            taskRepository.update(task);
            taskRepository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public @Nullable Task findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            return taskRepository.findById(userId, id);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public @Nullable void removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.removeById(userId, id);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String userId) {
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            return taskRepository.findAllById(userId);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public @Nullable List<Task> findAll() {
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            return taskRepository.findAll();
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.clear(userId);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            taskRepository.clear();
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void addAll(@Nullable List<Task> tasks) {
        if (tasks == null) return;
        @NotNull final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        try {
            taskRepository.getTransaction().begin();
            for (@NotNull final Task task : tasks) taskRepository.add(task);
            taskRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            taskRepository.getTransaction().rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}