package ru.tsc.avramenko.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.model.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.tsc.avramenko.tm")
public class ServerConfiguration {

    @Bean
    @Scope("prototype")
    public @NotNull EntityManager getEntityManager(EntityManagerFactory factory) {
        return factory.createEntityManager();
    }

    @Bean
    public EntityManagerFactory factory(@NotNull final IPropertyService propertyService) {
        @Nullable final String driver = propertyService.getJdbcDriver();
        if (driver == null) throw new ProcessException();
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new ProcessException();
        @Nullable final String password = propertyService.getJdbcPass();
        if (password == null) throw new ProcessException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new ProcessException();
        @Nullable final String dialect = propertyService.getHibernateDialect();
        if (dialect == null) throw new ProcessException();
        @Nullable final String auto = propertyService.getHibernateHbm2ddl();
        if (auto == null) throw new ProcessException();
        @Nullable final String sqlShow = propertyService.getHibernateShowSql();
        if (sqlShow == null) throw new ProcessException();

        @Nullable final String secondLevelCash = propertyService.getHibernateCacheUseSecondLevelCache();
        if (secondLevelCash == null) throw new ProcessException();
        @Nullable final String queryCache = propertyService.getHibernateCacheUseQueryCache();
        if (queryCache == null) throw new ProcessException();
        @Nullable final String minimalPuts = propertyService.getHibernateCacheUseMinimalPuts();
        if (minimalPuts == null) throw new ProcessException();
        @Nullable final String regionPrefix = propertyService.getHibernateCacheRegionPrefix();
        if (regionPrefix == null) throw new ProcessException();
        @Nullable final String cacheProvider = propertyService.getHibernateCacheProviderConfigurationFileResourcePath();
        if (cacheProvider == null) throw new ProcessException();
        @Nullable final String factoryClass = propertyService.getHibernateCacheRegionFactoryClass();
        if (factoryClass == null) throw new ProcessException();
        @Nullable final String liteMember = propertyService.getHibernateCacheHazelcastUseLiteMember();
        if (liteMember == null) throw new ProcessException();

        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, driver);
        settings.put(org.hibernate.cfg.Environment.URL, url);
        settings.put(org.hibernate.cfg.Environment.USER, username);
        settings.put(org.hibernate.cfg.Environment.PASS, password);
        settings.put(org.hibernate.cfg.Environment.DIALECT, dialect);
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, auto);
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, sqlShow);

        if ("true".equals(secondLevelCash)) {
            settings.put(org.hibernate.cfg.Environment.USE_SECOND_LEVEL_CACHE, secondLevelCash);
            settings.put(org.hibernate.cfg.Environment.USE_QUERY_CACHE, queryCache);
            settings.put(org.hibernate.cfg.Environment.USE_MINIMAL_PUTS, minimalPuts);
            settings.put(org.hibernate.cfg.Environment.CACHE_REGION_PREFIX, regionPrefix);
            settings.put(org.hibernate.cfg.Environment.CACHE_PROVIDER_CONFIG, cacheProvider);
            settings.put(org.hibernate.cfg.Environment.CACHE_REGION_FACTORY, factoryClass);
            settings.put("hibernate.cache.hazelcast.use_lite_member", liteMember);
        }

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);

        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}