package ru.tsc.avramenko.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.model.IUserRepository;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.api.service.model.IUserService;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.UserEmailExistsException;
import ru.tsc.avramenko.tm.exception.entity.UserLoginExistsException;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.repository.model.UserRepository;
import ru.tsc.avramenko.tm.service.AbstractService;
import ru.tsc.avramenko.tm.util.HashUtil;

import java.util.List;

@Service
public class UserService extends AbstractService implements IUserService {

    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            return repository.findById(id);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            return repository.findByLogin(login);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.removeUserByLogin(login);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.removeById(id);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USER);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.add(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USER);
        user.setFirstName("New");
        user.setMiddleName("User");
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.add(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.add(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(final String login) {
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            return repository.findByLogin(login) != null;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(final String email) {
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            return repository.findByEmail(email) != null;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isUserExist(final String id) {
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            return repository.findById(id) != null;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setRole(@Nullable final String id, @Nullable final Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUserById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUserByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void clear() {
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            repository.clear();
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public @Nullable List<User> findAll() {
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    public void addAll(@Nullable List<User> users) {
        if (users == null) return;
        @NotNull IUserRepository repository = context.getBean(UserRepository.class);
        try {
            repository.getTransaction().begin();
            for (@NotNull final User user : users) repository.add(user);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}