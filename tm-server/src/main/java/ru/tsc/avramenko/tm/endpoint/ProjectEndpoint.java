package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.avramenko.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.avramenko.tm.api.service.dto.IProjectDtoService;
import ru.tsc.avramenko.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.avramenko.tm.api.service.dto.ISessionDtoService;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.dto.ProjectDTO;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Autowired
    private ISessionDtoService sessionDtoService;

    @Autowired
    private IProjectDtoService projectDtoService;

    @Autowired
    private IProjectTaskDtoService projectTaskDtoService;

    @NotNull
    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionDtoService.validate(session);
        projectDtoService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public @Nullable List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.findAll(session.getUserId());

    }

    @NotNull
    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionDtoService.validate(session);
        projectDtoService.clear(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public @Nullable ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.findById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public @Nullable ProjectDTO findProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.findByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public @Nullable ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.findByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO finishProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.finishById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.finishByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO finishProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.finishByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        projectTaskDtoService.removeProjectById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        projectTaskDtoService.removeProjectByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        projectTaskDtoService.removeProjectByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public void setProjectStatusById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionDtoService.validate(session);
        projectDtoService.changeStatusById(session.getUserId(), id, status);
    }

    @NotNull
    @Override
    @WebMethod
    public void setProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionDtoService.validate(session);
        projectDtoService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @NotNull
    @Override
    @WebMethod
    public void setProjectStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionDtoService.validate(session);
        projectDtoService.changeStatusByName(session.getUserId(), name, status);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO startProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.startById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO startProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.startByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO startProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.startByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.updateById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionDtoService.validate(session);
        return projectDtoService.updateByIndex(session.getUserId(), index, name, description);
    }

}