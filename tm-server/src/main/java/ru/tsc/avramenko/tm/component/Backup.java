package ru.tsc.avramenko.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.service.IDataService;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import java.util.concurrent.*;

@Component
public class Backup implements Runnable {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    @NotNull
    public static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    @NotNull
    public static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data-jaxb.json";

    @NotNull
    public static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull
    public static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull
    public static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    @NotNull
    public static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    @NotNull
    public static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String BACKUP_XML = "./backup.xml";

    @Autowired
    private IDataService dataService;

    @NotNull
    private final ThreadFactory threadFactory = runnable -> new Thread(runnable, "DataThread");
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor(threadFactory);

    @Nullable
    private final int INTERVAL;

    public Backup(@NotNull final IPropertyService propertyService) {
        this.INTERVAL = propertyService.getBackupInterval();
    }

    public void init() {
        start();
    }

    @SneakyThrows
    public void load() {
        dataService.loadBackup();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    @Override
    @SneakyThrows
    public void run() {
        dataService.saveBackup();
    }

}