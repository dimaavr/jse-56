package ru.tsc.avramenko.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.api.service.dto.IUserDtoService;
import ru.tsc.avramenko.tm.dto.UserDTO;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.empty.*;
import ru.tsc.avramenko.tm.exception.entity.UserEmailExistsException;
import ru.tsc.avramenko.tm.exception.entity.UserLoginExistsException;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.repository.dto.UserDtoRepository;
import ru.tsc.avramenko.tm.service.AbstractService;
import ru.tsc.avramenko.tm.util.HashUtil;

import java.util.List;

@Service
public class UserDtoService extends AbstractService implements IUserDtoService {

    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            return userDtoRepository.findById(id);
        } finally {
            userDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            return userDtoRepository.findByLogin(login);
        } finally {
            userDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.removeUserByLogin(login);
            userDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.removeById(id);
            userDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USER);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.add(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USER);
        user.setFirstName("New");
        user.setMiddleName("User");
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.add(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.add(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.update(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(final String login) {
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            return userDtoRepository.findByLogin(login) != null;
        } finally {
            userDtoRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(final String email) {
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            return userDtoRepository.findByEmail(email) != null;
        } finally {
            userDtoRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isUserExist(final String id) {
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            return userDtoRepository.findById(id) != null;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setRole(@Nullable final String id, @Nullable final Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.update(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUserById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.update(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUserByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.update(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.update(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.update(user);
            userDtoRepository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @Override
    public void clear() {
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            userDtoRepository.clear();
            userDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

    @Override
    public @Nullable List<UserDTO> findAll() {
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            return userDtoRepository.findAll();
        } finally {
            userDtoRepository.close();
        }
    }

    @Override
    public void addAll(@Nullable List<UserDTO> users) {
        if (users == null) return;
        @NotNull IUserDtoRepository userDtoRepository = context.getBean(UserDtoRepository.class);
        try {
            userDtoRepository.getTransaction().begin();
            for (@NotNull final UserDTO user : users) userDtoRepository.add(user);
            userDtoRepository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            userDtoRepository.getTransaction().rollback();
            throw e;
        } finally {
            userDtoRepository.close();
        }
    }

}