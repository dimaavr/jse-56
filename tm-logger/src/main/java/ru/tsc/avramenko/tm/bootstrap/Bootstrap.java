package ru.tsc.avramenko.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.avramenko.tm.api.ILoggingService;
import ru.tsc.avramenko.tm.api.IPropertyService;
import ru.tsc.avramenko.tm.service.LoggingService;
import ru.tsc.avramenko.tm.service.PropertyService;

import javax.jms.JMSException;
import java.util.List;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggingService loggingService;

    public void run() {
        @NotNull final List<String> entities = propertyService.getLogEntities();
        entities.forEach(item -> {
            try {
                loggingService.createBroadcastConsumer(item);
            } catch (JMSException e) {
                e.fillInStackTrace();
            }
        });
    }

}