package ru.tsc.avramenko.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.avramenko.tm.api.ILoggingService;
import ru.tsc.avramenko.tm.api.IPropertyService;
import ru.tsc.avramenko.tm.listener.LoggerListener;

import javax.jms.*;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.util.Optional;

@Service
public class LoggingService implements ILoggingService {

    @Override
    public void createBroadcastConsumer(@NotNull final String className) throws JMSException {
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(className);
        @NotNull MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(new LoggerListener());
    }

    @Override
    @SneakyThrows
    public void writeLog(@NotNull TextMessage textMessage) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String message = textMessage.getText();
        @NotNull final ObjectNode node = objectMapper.readValue(message, ObjectNode.class);
        if (!node.has("className")) return;
        @Nullable final String className = node.get("className").asText();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @Nullable final String fileName = propertyService.getFilePath(className);
        if (!Optional.ofNullable(fileName).isPresent()) return;
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileWriter fileOutputStream = new FileWriter(file, true);
        fileOutputStream.write(message);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}